﻿using PathCreation;
using PathCreation.Examples;
using UnityEngine;

namespace Obstacles
{
    public class ObstaclesGenerator : PathSceneTool
    {
        [SerializeField] private GameObject[] _obstaclePrefabs;
        [SerializeField] private GameObject _finishPrefab;
        [SerializeField] private GameObject _holder;

        public float DistanceWithoutObstacles = 10f;
        public float Spacing = 3;
        const float MinSpacing = .1f;

        void Generate () {
            if (pathCreator != null && _obstaclePrefabs.Length > 0 && _holder != null) {
                DestroyObjects ();

                VertexPath path = pathCreator.path;

                Spacing = Mathf.Max(MinSpacing, Spacing);
                float dst = 0;
                
                while (dst < path.length) {
                    if (dst < DistanceWithoutObstacles)
                        dst += Spacing;
                    else
                    {
                        int index = 0;
                        var prefab = GetRandomObstacle(out index);
                        SpawnObject(index, dst, prefab);
                        dst += Spacing;
                    }
                }
                
                SpawnObject(0, path.length - 0.01f, _finishPrefab);
            }
        }

        private void SpawnObject(int index, float distance, GameObject prefab)
        {
            Vector3 point = path.GetPointAtDistance (distance);
            // Quaternion rot = path.GetRotationAtDistance (distance);
            Quaternion rot = new Quaternion();

            if (index != 0)
            {
                var xPos = Random.Range(-2f, 2f);
                var randomOffsetPosition = new Vector3(xPos, 0f, 0f);
                Instantiate(prefab, point + randomOffsetPosition, rot, _holder.transform);
            }
            else
                Instantiate (prefab, point, rot, _holder.transform);
        }
        
        private void DestroyObjects () {
            int numChildren = _holder.transform.childCount;
            for (int i = numChildren - 1; i >= 0; i--) {
                DestroyImmediate(_holder.transform.GetChild (i).gameObject, false);
            }
        }

        private GameObject GetRandomObstacle(out int index)
        {
            var maxIndex = _obstaclePrefabs.Length;
            index = Random.Range(0, maxIndex);
            
            return _obstaclePrefabs[index];
        }

        protected override void PathUpdated () {
            if (pathCreator != null) {
                Generate ();
            }
        }
    }
}