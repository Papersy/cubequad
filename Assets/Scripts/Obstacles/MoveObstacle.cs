﻿using UnityEngine;

namespace Obstacles
{
    public class MoveObstacle : MonoBehaviour
    {
        [SerializeField] private float _speed = 2.0f;
        [SerializeField] private float _minX = -2.5f;
        [SerializeField] private float _maxX = 2.5f;
        
        void Update()
        {
            Vector3 currentPosition = transform.position;
            
            float targetX = Mathf.Lerp(_minX, _maxX, (Mathf.Sin(Time.time * _speed) + 1) / 2);
            float newX = Mathf.Clamp(targetX, _minX, _maxX);

            transform.position = new Vector3(newX, currentPosition.y, currentPosition.z);
        }
    }
}