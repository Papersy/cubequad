﻿using Data;
using Infrastructure.Services;

namespace StaticData
{
    public interface IStaticDataService : IService
    {
        public T GetData<T>() where T : BaseData;
    }
}