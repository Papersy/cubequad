﻿using Infrastructure.Services;
using Infrastructure.Services.Data;
using StaticData;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    private GameObject _levelObj;
    private LevelsSo _levelsSo;
    
    private void Awake()
    {
        _levelsSo = AllServices.Container.Single<IStaticDataService>().GetData<LevelsSo>();
        
        GenerateLevel();
    }

    public void GenerateLevel()
    {
        if(_levelObj != null)
            Destroy(_levelObj.gameObject);

        var currentLevel = AllServices.Container.Single<IDataService>().PlayerDynamicData.PlayerProgressData.CurrentLevel;
        var prefab = _levelsSo.GetLevelPrefab(currentLevel);

        _levelObj = Instantiate(prefab);
    }
}