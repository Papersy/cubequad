﻿using System;
using Infrastructure.Services.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class MainWindow : BaseWindow
    {
        [SerializeField] private Button _restart;
        [SerializeField] private TextMeshProUGUI _currentLevel;
        
        public event Action OnRestart;
        
        private void OnEnable()
        {
            _restart.onClick.AddListener(OnBtnNext);
            
            PlayerDynamicData.PlayerProgressData.OnLevelUpdate += UpdateLevelText;
        }

        private void OnDisable()
        {
            _restart.onClick.RemoveListener(OnBtnNext);
            
            PlayerDynamicData.PlayerProgressData.OnLevelUpdate -= UpdateLevelText;
        }

        private void UpdateLevelText(int level) => _currentLevel.text = (level + 1).ToString();
        
        private void OnBtnNext() => OnRestart?.Invoke();
    }
}