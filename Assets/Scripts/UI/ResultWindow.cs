﻿
using System;
using Infrastructure.Services.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class ResultWindow : BaseWindow
    {
        [SerializeField] private Button _next;
        [SerializeField] private TextMeshProUGUI _resultText;
        
        public event Action<bool> OnRestart;

        public void Lose()
        {
            Show();
            OnBtnNext(false);
            _resultText.text = "You lost, try again:(";
        }

        public void Victory()
        {
            Show();
            OnBtnNext(true);
            _resultText.text = "You win, let's play next!";
        }

        private void OnBtnNext(bool status)
        {
            _next.onClick.RemoveAllListeners();
            _next.onClick.AddListener(() =>
            {
                Hide();
                OnRestart?.Invoke(status);
            });
        }
    }
}