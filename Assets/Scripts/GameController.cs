﻿using System.Collections;
using Cinemachine;
using Hero;
using Infrastructure.Input;
using Infrastructure.Services;
using Infrastructure.Services.Data;
using Infrastructure.Services.UI;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera _camera;
    [SerializeField] private HeroMovement _heroMovement;
    [SerializeField] private LevelGenerator _levelGenerator;

    public static GameController Instance;
    public static bool IsGame = false;
    
    private IInputService _inputService;
    private GameUICanvas _gameUICanvas;
    
    private void Awake()
    {
        Instance = this;
        
        _inputService = AllServices.Container.Single<IInputService>();
        _gameUICanvas = AllServices.Container.Single<IUIService>().HudContainer.GameCanvas;
    }
    
    private void OnEnable()
    {
        _inputService.OnEventDown += StartGame;
        _gameUICanvas.ResultWindow.OnRestart += OnResultRestart;
        _gameUICanvas.MainWindow.OnRestart += OnMainRestart;
    }

    private void OnDisable()
    {
        _inputService.OnEventDown -= StartGame;
        _gameUICanvas.ResultWindow.OnRestart -= OnResultRestart;
        _gameUICanvas.MainWindow.OnRestart -= OnMainRestart;
    }

    private void StartGame() => IsGame = true;

    public void Lose()
    {
        IsGame = false;
        GameUICanvas.Instance.ResultWindow.Lose();
    }

    public void Victory()
    {
        IsGame = false;
        _heroMovement.TurnVictoryAnimation();
        AllServices.Container.Single<IDataService>().PlayerDynamicData.PlayerProgressData.FinishLevel();

        StartCoroutine(ShowVictoryUI());
    }
    
    private void OnResultRestart(bool status)
    {
        IsGame = false;
        _heroMovement.ResetHero();
        
        if(status)
            _levelGenerator.GenerateLevel();
    }

    private void OnMainRestart()
    {
        IsGame = false;
        _heroMovement.ResetHero();
    }

    private IEnumerator ShowVictoryUI()
    {
        yield return new WaitForSeconds(2f);
        
        GameUICanvas.Instance.ResultWindow.Victory();
    }
}