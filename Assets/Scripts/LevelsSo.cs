﻿using System;
using System.Collections.Generic;
using System.Linq;
using Data;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelsSo", menuName = "ScriptableObjects/LevelsSo", order = 1)]
public class LevelsSo : BaseData
{
    public List<LevelSettings> Levels = new();

    public GameObject GetLevelPrefab(int id)
    {
        foreach (var level in Levels.Where(level => level.Id == id))
            return level.LevelPrefab;

        var index = id % Levels.Count;

        return Levels[index].LevelPrefab;
    }
}

[Serializable]
public class LevelSettings
{
    public int Id;
    public GameObject LevelPrefab;
}