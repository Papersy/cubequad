﻿using Infrastructure.AssetManagement;
using UnityEngine;

namespace Infrastructure.Factory
{
    public class GameFactory : IGameFactory
    {
        public GameObject Hero { get; set; }

        private readonly IAssetProvider _assets;

        public GameFactory(IAssetProvider assetsProvider) =>
            _assets = assetsProvider;

        public void Cleanup()
        {
        }

        public GameObject Instantiate(string prefabPath)
        {
            GameObject gameObject = Object.Instantiate(_assets.Instantiate(prefabPath));

            return gameObject;
        }

        public GameObject InstantiateHero(string prefabPath)
        {
            Vector3 pos;

            if (Hero != null)
                pos = Hero.transform.position;
            else
                pos = Vector3.zero;

            GameObject gameObject = Object.Instantiate(_assets.Instantiate(prefabPath, pos));

            if (Hero != null)
                GameObject.Destroy(Hero);

            Hero = gameObject;

            return gameObject;
        }
    }
}