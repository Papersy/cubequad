using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Infrastructure
{
    public class HUDLoading : MonoBehaviour
    {
        [SerializeField] private Slider progressSlider;
        [SerializeField] private TextMeshProUGUI loadingText;
        [SerializeField] private float speed;

        private float endValue;
        private Action callback;
        private AsyncOperation waitNextScene;

        private void Update()
        {
            endValue = waitNextScene.progress * 100f;

            UpdateProgress(Mathf.MoveTowards(progressSlider.value, endValue, speed * Time.deltaTime));

            if (progressSlider.value >= 100)
                OnFinishLoad();
        }

        public void StartLoad(AsyncOperation waitNextScene, Action callback)
        {
            this.callback = callback;
            this.waitNextScene = waitNextScene;

            progressSlider.value = 0;
            gameObject.SetActive(true);
        }

        private void UpdateProgress(float progress)
        {
            progressSlider.value = progress;
            loadingText.text = $"Loading... {progress:F0}%";
        }

        private void OnFinishLoad()
        {
            callback?.Invoke();
            gameObject.SetActive(false);
        }
    }
}