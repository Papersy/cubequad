﻿using System.Threading.Tasks;

namespace Infrastructure.Services.Data
{
    public interface ISaveProgress
    {
        public void SaveProgress();
        public Task ReproduceProgress();
    }
}