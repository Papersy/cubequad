﻿using System;

namespace Data
{
    [Serializable]
    public class PlayerDynamicData
    {
        public PlayerProgressData playerProgressData;
        public PlayerResourcesData playerResourcesData;

        public PlayerProgressData PlayerProgressData => playerProgressData;
        public PlayerResourcesData PlayerResourcesData => playerResourcesData;

        public PlayerDynamicData()
        {
            playerProgressData = new PlayerProgressData();
            playerResourcesData = new PlayerResourcesData();
        }
    }
}