﻿using System;

namespace Data
{
    [Serializable]
    public class PlayerProgressData
    {
        public int CurrentLevel = 0;

        public event Action<int> OnLevelUpdate;

        public void FinishLevel()
        {
            CurrentLevel++;
            
            OnLevelUpdate?.Invoke(CurrentLevel);
        }
    }
}