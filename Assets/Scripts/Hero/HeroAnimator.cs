using UnityEngine;

namespace Hero
{
    public class HeroAnimator : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        
        private static readonly int MoveForward = Animator.StringToHash("MoveForward");
        private static readonly int Dance = Animator.StringToHash("Dance");
        private static readonly int Idle = Animator.StringToHash("Idle");

        public void ResetAnimations()
        {
            _animator.SetBool(MoveForward, false);
            _animator.SetBool(Dance, false);
        }
        
        public void StartMoveAnimation() => _animator.SetBool(MoveForward, true);
        public void StartDanceAnimation() => _animator.SetBool(Dance, true);
    }
}
