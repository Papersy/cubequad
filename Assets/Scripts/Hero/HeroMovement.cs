using Infrastructure.Input;
using Infrastructure.Services;
using UnityEngine;

namespace Hero
{
    public class HeroMovement : MonoBehaviour
    {
        [SerializeField] private float _horizontalSpeed;
        [SerializeField] private float _forwardSpeed;
        [SerializeField] private HeroAnimator _heroAnimator;

        public CharacterController CharacterController;
        
        public static bool CanMove { get; set; } = true;

        private IInputService _inputService;

        private void OnEnable()
        {
            _inputService.OnEventDown += FingerClick;
            _inputService.OnEventDrag += ChangeHorizontalCoords;
        }

        private void OnDisable()
        {
            _inputService.OnEventDown -= FingerClick;
            _inputService.OnEventDrag -= ChangeHorizontalCoords;
        }

        private void Awake() => _inputService = AllServices.Container.Single<IInputService>();

        private void Update()
        {
            if (!GameController.IsGame)
                return;

            Vector3 movementVector = Vector3.forward;
            movementVector += Physics.gravity;

            CharacterController.Move(_forwardSpeed * Time.deltaTime * movementVector);
        }

        public void ResetHero()
        {
            _heroAnimator.ResetAnimations();
            
            CharacterController.enabled = false;
            transform.position = Vector3.zero;
            CharacterController.enabled = true;
        }

        public void TurnVictoryAnimation() => _heroAnimator.StartDanceAnimation();

        private void FingerClick()
        {
            CanMove = true;
            _heroAnimator.StartMoveAnimation();
        }
        
        private void ChangeHorizontalCoords(float value)
        {
            var xPos = transform.position.x;
            var newValue = Mathf.Clamp(value, -1f, 1f);

            if (newValue >= 0 && xPos < 2.4f)
                CharacterController.Move(Vector3.right * Time.deltaTime * _horizontalSpeed);
            else if(newValue < 0 && xPos > -2.4f)
                CharacterController.Move(Vector3.left * Time.deltaTime * _horizontalSpeed);
        }
    }
}