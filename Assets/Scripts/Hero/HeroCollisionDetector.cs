﻿using UnityEngine;

namespace Hero
{
    public class HeroCollisionDetector : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Finish"))
            {
                Debug.Log("Finish");
                GameController.Instance.Victory();
            }
            else if (other.CompareTag("Obstacle"))
            {
                Debug.Log("Obstacles");
                GameController.Instance.Lose();
            }
        }
    }
}