﻿using UnityEngine;

namespace Hero
{
    public class PlayerFollower : MonoBehaviour
    {
        [SerializeField] private Transform _target;

        private void LateUpdate()
        {
            transform.position = new Vector3(0f, _target.position.y, _target.position.z);
        }
    }
}